﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task2.MatrixStep
{
    class Program
    {

        static int GetConvertNumber(string columnsOrLines)
        {
            Console.WriteLine(columnsOrLines);
            string value = Console.ReadLine();
            int result;
            bool checkError = int.TryParse(value, out result);
            while (!checkError)
            {
                Console.WriteLine("Input Error. "+ columnsOrLines);
                checkError = int.TryParse(Console.ReadLine(), out result);
            }
            return result;
        }

        static void Main(string[] args)
        {
            int linesCount = GetConvertNumber("Write matrix number of lines:");
            int columnsCount = GetConvertNumber("Write matrix number of columns:");
            Matrix matr = new Matrix(linesCount, columnsCount);
            Console.WriteLine("Your matrix:");
            matr.PrintMatrix();
            Console.WriteLine("Matrix step is: " + matr.GetMatrixTrace());
            Console.WriteLine("Print matrix by snake ");
            matr.PrintElementsBySpiral();
           
        }
    }
}
