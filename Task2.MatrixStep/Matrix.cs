﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task2.MatrixStep
{
    public class Matrix
    {
       private int[,] matrix;

        public  Matrix(int lines, int columns)
        {
            matrix = new int[lines, columns];
            Random rand = new Random();
            for (int i = 0; i < lines; i++)
            {
                for (int j = 0; j < columns; j++)
                {
                    matrix[i, j] = rand.Next(0, 101);
                }
            }
        }

        public Matrix(int[,] array )
        {
            matrix = array;
        }

        public int GetMatrixTrace()
        {
            int result = 0;
            for (int i = 0; i < this.matrix.GetLength(0); i++)
            {
                for (int j = 0; j < this.matrix.GetLength(1); j++)
                {
                    if (i == j)
                    {
                        result += this.matrix[i, j];
                    }
                }
            }
            return result;
        }

        public  void PrintMatrix()
        {
            for (int i = 0; i < matrix.GetLength(0); i++)
            {
                for (int j = 0; j < matrix.GetLength(1); j++)
                {
                    if (i == j)
                    {
                        Console.ForegroundColor = ConsoleColor.Red;
                        Console.Write(matrix[i, j] + "\t");
                        Console.ForegroundColor = ConsoleColor.White;
                    }
                    else
                    {
                        Console.Write(matrix[i, j] + "\t");
                    }
                }
                Console.WriteLine();
            }
        }

        public void PrintElementsBySpiral()
        {
            int n = matrix.GetLength(0);
            int count = n;
            int value = -n;
            int sum = -1;

            do
            {
                value = -1 * value / n;
                for (int i = 0; i < count; i++)
                {
                    sum += value;
                    Console.Write(matrix[sum / n, sum % n] + " ");
                }
                value *= n;
                count--;
                for (int i = 0; i < count; i++)
                {
                    sum += value;
                    Console.Write(matrix[sum / n, sum % n] + " ");
                }
            } while (count > 0);
        }

        public int[,] GetMatrix()
        {
            return matrix;
        }



    }
}
