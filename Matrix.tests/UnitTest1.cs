using Microsoft.VisualStudio.TestTools.UnitTesting;
using Task2.MatrixStep;

namespace Matrix.tests
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void GetMatrix_check_random_ellemets()
        {
            //arrange
            int lines = 4;
            int columns = 4;
            int[,] matrix;
            bool expected = true;

            //act
            Task2.MatrixStep.Matrix matr= new Task2.MatrixStep.Matrix(lines, columns);
            matrix = matr.GetMatrix();
            bool actual = false ;
            for(int i = 0; i < lines/2; i++)
            {
                for(int j = 0; j < columns/2; j++)
                {
                    if (matrix[i, j] == matrix[i, j + 1] && matrix[i, j + 1] == matrix[i, j + 2])
                    {
                        actual = false;
                        break;
                    }else if(matrix[i, j] == matrix[i + 1, j] && matrix[i + 1, j] == matrix[i + 2, j])
                    {
                        actual = false;
                        break;
                    }
                    else
                    {
                        actual = true;
                    }
                }
            }

            //assert
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void GetMatrix_check_matrix_values()
        {
            //arrange
            int columns = 4, lines = 4;
            int[,] matrix = new int [lines,columns];
            bool expected = true;
            int value = 1;

            //act
            bool actual = true;
            for(int i = 0; i < lines; i++)
            {
                for(int j = 0; j < columns; j++)
                {
                    matrix[i, j] = value;
                }
            }
            Task2.MatrixStep.Matrix matr = new Task2.MatrixStep.Matrix(matrix);
            int[,] returnedMatrix = matr.GetMatrix();
            for(int i = 0; i < lines; i++)
            {
                for(int j = 0; j < columns; j++)
                {
                    if(matrix[i,j] != returnedMatrix[i, j])
                    {
                        actual = false;
                    }
                }
            }

            //assert
            Assert.AreEqual(expected, actual);

        }

        [TestMethod]
        public void GetMatrixTrace_4_4returned()
        {
            //arrange
            int lines = 4, columns = 4;
            int[,] matrix = new int[lines, columns];
            int value = 1;
            int expected = 4;

            //act
            for(int i = 0; i < lines; i++)
            {
                for(int j = 0; j < columns; j++)
                {
                    matrix[i, j] = value;
                }
            }
            Task2.MatrixStep.Matrix matr = new Task2.MatrixStep.Matrix(matrix);
            int actual = matr.GetMatrixTrace();

            //assert
            Assert.AreEqual(expected, actual);

        }

        [TestMethod]
        public void GetMatrix_checkSize_5x5_5x5returned()
        {
            //arrange
            int lines = 5, columns = 5;
            int[,] matrix;
            bool expected = true;

            //act
            bool actual = false;
            Task2.MatrixStep.Matrix matr = new Task2.MatrixStep.Matrix(lines, columns);
            matrix = matr.GetMatrix();
            if(matrix.GetLength(0) == lines && matrix.GetLength(1) == columns)
            {
                actual = true;
            }

            //assert
            Assert.AreEqual(expected, actual);

        }
    }
}
